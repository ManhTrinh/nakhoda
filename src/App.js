import React, { Component } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

import Navigation from './components/Navigation';

import IsteNatus from './components/IsteNatus';
import Explicabo from './components/Explicabo';
import Omnis from './components/Omnis';
import IpsaQuae from './components/IpsaQuae';
import PerspiciatisUnde from './components/PerspiciatisUnde';
import Error from './components/Error';

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <div id="main-layout">
          <Navigation />
          <main>
            <Switch>
                <Route path="/" component={IsteNatus} exact />
                <Route path="/Explicabo" component={Explicabo} />
                <Route path="/Omnis" component={Omnis} />
                <Route path="/IpsaQuae" component={IpsaQuae} />
                <Route path="/PerspiciatisUnde" component={PerspiciatisUnde} />
                <Route component={Error} />
            </Switch>
          </main>
        </div>
      </BrowserRouter>
    );
  }
}

export default App;
