import React, { Component } from 'react';

class Error extends Component {
  render() { 
    return (
      <div>Error - Page not found !</div>
    );
  }
}
 
export default Error;
