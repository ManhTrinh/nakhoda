import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';

class Omnis extends Component {
  render() { 
    return (
      <div>Omnis Page <br/>
      Please visit <NavLink to="/PerspiciatisUnde">Perspiciatis Unde</NavLink> Page to see the content in the assignment.</div>
    );
  }
}
 
export default Omnis;
