import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';

class Explicabo extends Component {
  render() { 
    return (
      <div>Explicabo Page <br/>
      Please visit <NavLink to="/PerspiciatisUnde">Perspiciatis Unde</NavLink> Page to see the content in the assignment.</div>
    );
  }
}
 
export default Explicabo;
