import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';

class IpsaQuae extends Component {
  render() { 
    return (
      <div>IpsaQuae Page <br/>
      Please visit <NavLink to="/PerspiciatisUnde">Perspiciatis Unde</NavLink> Page to see the content in the assignment.</div>
    );
  }
}
 
export default IpsaQuae;
