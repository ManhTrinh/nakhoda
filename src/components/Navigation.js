import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import './Navigation.scss';
import {
  IoIosArrowDropdownCircle,
  IoMdTrash,
  IoMdMail,
  IoIosArrowDown
} from 'react-icons/io';

class Navigation extends Component {
  constructor(props) {
    super(props);
    this.state = { 
      aperiam: false,
      totam: true      
    }
  }

  render() { 
    return (
      <nav>
        <div className="nav-top">
          <ul className="main-menu">
            <li><NavLink to="/" exact>Iste Natus</NavLink></li>
            <li><NavLink to="/Explicabo">Explicabo</NavLink></li>
            <li><NavLink to="/Omnis">Omnis</NavLink></li>
            <li><NavLink to="/IpsaQuae">Ipsa quae</NavLink></li>
            <li><NavLink to="/PerspiciatisUnde">Perspiciatis Unde</NavLink></li>
          </ul>
          <div className="secondary-menu">
            <div className="dropdown-wrapper">
              <div className="dropdown-label">
                <span>Veris Veritatis</span>
                <IoIosArrowDown />
              </div>
              <div className="dropdown-menu">
                <ul>
                  <li><a href="#">Actionmenu1</a></li>
                  <li><a href="#">Actionmenu2</a></li>
                  <li><a href="#">Actionmenu3</a></li>
                  <li><a href="#">Actionmenu4</a></li>
                </ul>
              </div>
            </div>
            <button type="button" className="menu-profile">
              Z
            </button>
          </div>
        </div>
        <div className="nav-bottom">
          <div className="nav-bottom-left">
            <div>Sunt</div>
            <button>
              <IoIosArrowDropdownCircle />
              <span>Beatae Vita</span>
            </button>
            <div>Doloremque laudantium otam Doloremque laudantium otam</div>
          </div>
          <div className="nav-bottom-right">
            <button>
              <IoMdTrash />
              <span>Archieto</span>
            </button>
            <div className="custom-switch mr30">
              <label>
                <input type="checkbox" defaultChecked={this.state.aperiam}/>
                <span className="switch-shape"></span>
                <span>Aperiam</span>
              </label>
            </div>
            <div className="custom-switch mr30">
              <label>
                <input type="checkbox" defaultChecked={this.state.totam}/>
                <span className="switch-shape"></span>
                <span>Totam</span>
              </label>
            </div>
            <button>
              <IoMdMail />
              <span>Rem</span>
            </button>
          </div>
        </div>
      </nav>
    );
  }
}
 
export default Navigation;
