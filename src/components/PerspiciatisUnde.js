import React, { Component } from 'react';
import './PerspiciatisUnde.scss';

import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';

import {
  IoIosCheckmark,
  IoIosArrowDown
} from 'react-icons/io';

class PerspiciatisUnde extends Component {
  constructor(props) {
    super(props);
    this.state = { 
      selectedItemFromList: 0,
      selectList: [
        { name: 'Accusantium doloremque laudant', value: 0},
        { name: 'Architecto beatae vitae', value: 1},
        { name: 'Architecto beatae vitae', value: 2},
        { name: 'Architecto beatae vitae', value: 3},
        { name: 'Architecto beatae vitae', value: 4},
        { name: 'Architecto beatae vitae', value: 5},
        { name: 'Architecto beatae vitae', value: 6},
        { name: 'Architecto beatae vitae', value: 7},
      ],
      activeTab: 1
    }
  }

  changeActiveTab = (tabToShow) => {
    this.setState({
      activeTab: tabToShow
    });
  };

  handleChange = event => {
    this.setState({ selectedItemFromList: event.target.value });
  };

  tabToShow() {
    if (this.state.activeTab === 0) {
      return (<div>Tab 1</div>);
    } else if (this.state.activeTab === 1) {
      return (
        <div className="flex ai-baseline tab2">
          <div className="cl-secondary">Iste natus</div>
          <div className="custom-select">
            <Select
              className="select"
              value={this.state.selectedItemFromList}
              onChange={this.handleChange}
              name="select"
            >
              { this.state.selectList.map(item => <MenuItem value={item.value} key={item.value}>{ item.name }</MenuItem>) }
            </Select>
            <IoIosArrowDown />
          </div>
          <div className="cl-primary"><b>Unde omnis</b></div>
        </div>
      );
    } else if (this.state.activeTab === 3) {
      return (<div>Tab 3</div>);
    }
  }
  render() { 
    return (
      <div className="container perspiciatis-unde-page">
        <h3 className="flex ai-center">
          <IoIosCheckmark />
          CSed ut perspiciatis unde
        </h3>

        <p className="mt6 mb30">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, <br/>
        eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.
        </p>

        <div className="middle-section fz14">
          <div className="flex m-12 mb24">
            <div className="col-sm-3">
              <ul>
                <li>Otam rem aperiam</li>
                <li>Ipsa quae</li>
                <li>Unde omnis</li>
                <li>Voluptatem (duvrnem)</li>
              </ul>
            </div>
            <div className="col-sm-3">
              <ul>
                <li>Perspiciatis Unde</li>
                <li>Architecto beatae vitae</li>
                <li>Perspiciatis Unde</li>
                <li>Architecto beatae vitae</li>
              </ul>
            </div>
            <div className="col-sm-6 flex column">
              <ul className="mt-auto">
                <li>architecto beatae vitae</li>
              </ul>
            </div>
          </div>

          <div className="flex m-12">
            <div className="col-sm-3">
              <ul>
                <li>Totam</li>
                <li>Iste Natus</li>
                <li>Omnis Iste Natus Erro</li>
                <li>Explicabo</li>
              </ul>
            </div>
            <div className="col-sm-3">
              <ul>
                <li>5.0</li>
                <li>Omnis iste natus</li>
                <li>Accusantium doloremque laudant</li>
                <li>Accusantium dolor</li>
              </ul>
            </div>
          </div>
        </div>
        <div className="selection-section tab">
          <div className="selection-top">
            <button
              type="button"
              onClick={() => this.changeActiveTab(0)}
              className={this.state.activeTab === 0 ? 'active' : ''}
            >Perspiciatis Unde</button>
            <button
              type="button"
              onClick={() => this.changeActiveTab(1)}
              className={this.state.activeTab === 1 ? 'active' : ''}
            >Accusantium doloremque laudant</button>
            <button
              type="button"
              disabled
            >Voluptatem (duvrnem)</button>
          </div>
          <div className="selection-bottom">
            {
             this.tabToShow()
            }
          </div>
        </div>
      </div>
    );
  }
}
 
export default PerspiciatisUnde;
